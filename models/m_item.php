<?php
    class CMItem{
        private $pdo = null;
        private $stmt = null;

        function __construct(){
            try {
                $this->pdo = new PDO(
                    "mysql:host=" . MYSQL_HOST . ";dbname=" . MYSQL_DATABASE, //DSN
                    MYSQL_USER, //Username
                    MYSQL_PASSWORD, //Password 
                    [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                        PDO::ATTR_EMULATE_PREPARES => false,
                    ]
                );
            } catch (Exception $ex) { die($ex->getMessage()); }
        }

        function __destruct(){
            if ($this->stmt!==null) { $this->stmt = null; }
            if ($this->pdo!==null) { $this->pdo = null; }
        }

        function getAllItems(){
            $sql = "SELECT * FROM `items`  ORDER BY `id` DESC";
            try {
                $this->stmt = $this->pdo->prepare($sql);
                $this->stmt->execute();
                $row   = $this->stmt->fetchAll();
            } catch (Exception $ex) { die($ex->getMessage()); }
            $this->stmt = null;
            return $row;
        }

        function insertItem($user,$title, $description,$url){
            $data = [
                'user' => $user,
                'title' => $title,
                'description' => $description,
                'url' => $url,
            ];
            $sql = "INSERT INTO items (user, title, description, url) VALUES (:user, :title, :description, :url)";
            try{
                $this->stmt = $this->pdo->prepare($sql);
                $this->stmt->execute($data);
                return 0;
            } catch (Exception $ex) { 
                return 1;
                die($ex->getMessage()); 
            }
        }
    }
?>