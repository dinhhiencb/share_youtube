<?php
    class CMUser{
        private $pdo = null;
        private $stmt = null;

        function __construct(){
            try {
                $this->pdo = new PDO(
                    "mysql:host=" . MYSQL_HOST . ";dbname=" . MYSQL_DATABASE, //DSN
                    MYSQL_USER, //Username
                    MYSQL_PASSWORD, //Password 
                    [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                        PDO::ATTR_EMULATE_PREPARES => false,
                    ]
                );
            } catch (Exception $ex) { die($ex->getMessage()); }
        }

        function __destruct(){
            if ($this->stmt!==null) { $this->stmt = null; }
            if ($this->pdo!==null) { $this->pdo = null; }
        }

        function loginUser($username,$password){
            $sql = "SELECT `id`, `username`, `email` FROM `users` WHERE `username` = :username and `password` = :password";
            try {
                $this->stmt = $this->pdo->prepare($sql);
                $this->stmt->bindParam('username', $username, PDO::PARAM_STR);
                $this->stmt->bindValue('password', md5($password), PDO::PARAM_STR);
                $this->stmt->execute();
                $count = $this->stmt->rowCount();
                $row   = $this->stmt->fetch(PDO::FETCH_ASSOC);
            } catch (Exception $ex) { die($ex->getMessage()); }
            $this->stmt = null;
            return $row;
        }

        function checkExistUser($username,$email){
            $sql = "SELECT * FROM `users` WHERE `username` = :username or `email` = :email";
            try {
                $this->stmt = $this->pdo->prepare($sql);
                $this->stmt->bindParam('username', $username, PDO::PARAM_STR);
                $this->stmt->bindValue('email', md5($email), PDO::PARAM_STR);
                $this->stmt->execute();
                $count = $this->stmt->rowCount();
                $row   = $this->stmt->fetch(PDO::FETCH_ASSOC);
            } catch (Exception $ex) { die($ex->getMessage()); }
            $this->stmt = null;
            return $row;
        }

        function regiserUser($username,$password,$email){
            $data = [
                'username' => $username,
                'password' => md5($password),
                'email' => $email,
            ];
            $sql = "INSERT INTO users (username, password, email) VALUES (:username, :password, :email)";
            try{
                $this->stmt = $this->pdo->prepare($sql);
                $this->stmt->execute($data);
                return 0;
            } catch (Exception $ex) { 
                return 1;
                die($ex->getMessage()); 
            }
        }
    }
?>