<!DOCTYPE html>
<html>
<head>
  <title>Login-Share Youtube videos</title>
  <link rel="stylesheet" type="text/css" href="resources/style.css">
</head>
<body>
  <div class="header">
  	<h2>Login</h2>
  </div>
	 
<?php 
	include('../config/define.php');
	include('../models/m_user.php');
	include('../controllers/ctr_user.php');
	$user = new CUser(); 
?>
  <form method="post" action="login.php">
  <?php 
	  		function showErr($msg){
				echo("<div class=\"error\">");
				echo("<p>".$msg."</p>");
				echo("</div");
			}
		 	if(isset($_POST['login_user'])){
			  $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
			  $password = !empty($_POST['password']) ? trim($_POST['password']) : null;
			  if($user->login($username,$password)){				  
					//set new session
					session_start();
					$_SESSION[SESSION_USER] = $username;
					$_SESSION[SESSION_TIME_LOGGIN] = time();
					header ("location: main.php");
				}
				else{
					showErr("Wrong username/password combination");
				}
            }
	   ?>
  	<div class="input-group">
  		<label>Username</label>
  		<input type="text" name="username" >
  	</div>
  	<div class="input-group">
  		<label>Password</label>
  		<input type="password" name="password">
  	</div>
  	<div class="input-group">
  		<button type="submit" class="btn" name="login_user">Login</button>
  	</div>
  	<p>
  		Not yet a member? <a href="regiser.php">Sign up</a>
  	</p>
  </form>
</body>
</html>