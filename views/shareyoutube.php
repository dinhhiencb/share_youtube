<?php 
	include('../config/define.php');
	include('../models/m_item.php');
	include('../controllers/ctr_item.php');
	$item = new CItem(); 
?>
<html>
<head>
  <title>Share youtube video</title>
  <link rel="stylesheet" type="text/css" href="resources/style.css">
</head>
<body>
  <div class="header">
  	<h2>Share Youtube Video</h2>
  </div>
	 
  <form method="post" action="shareyoutube.php">
  <?php 
	  		function showErr($msg){
				echo("<div class=\"error\">");
				echo("<p>".$msg."</p>");
				echo("</div");
			}
		 	if(isset($_POST['sharevideo'])){
			  $title = !empty($_POST['title']) ? trim($_POST['title']) : null;
			  $description = !empty($_POST['description']) ? trim($_POST['description']) : null;
			  $url = !empty($_POST['url']) ? trim($_POST['url']) : null;
			  session_start();
			  $result =$item->insert($_SESSION[SESSION_USER],$title, $description,$url);
			  if($result >0){
				switch($result){
					case 1:
						showErr("Please input full information");
						break;
					case 2:
						showErr("Unsuccess to share video");
						break;
					default:
						showErr("Error system, please try again");
						break;
					}
				}
				else{
					//move to index.
					header('location: main.php');
				}
            }
	   ?>
  	<div class="input-group">
  		<label>Title</label>
  		<input type="text" name="title" >
  	</div>
  	<div class="input-group">
  		<label>Description</label>
  		<textarea rows="4" type="text" name="description"></textarea>
	  </div>
	<div class="input-group">
  		<label>Url video</label>
  		<input type="text" name="url">
  	</div>
  	<div class="input-group">
  		<button type="submit" class="btn" name="sharevideo">Share</button>
  	</div>
  </form>
</body>
</html>