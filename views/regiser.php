<?php 
	include('../config/define.php');
	include('../models/m_user.php');
	include('../controllers/ctr_user.php');
	$user = new CUser(); 
?>
<!DOCTYPE html>
<html>
<head>
  <title>Register-Share Youtube videos</title>
  <link rel="stylesheet" type="text/css" href="resources/style.css">
</head>
<body>
  <div class="header">
  	<h2>Register</h2>
  </div>
	
  <form method="post" action="regiser.php">
	  <?php 
	  		function showErr($msg){
				echo("<div class=\"error\">");
				echo("<p>".$msg."</p>");
				echo("</div");
			}
		 	if(isset($_POST['reg_user'])){
			  $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
			  $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
			  $password = !empty($_POST['password']) ? trim($_POST['password']) : null;
			  $re_password = !empty($_POST['re_password']) ? trim($_POST['re_password']) : null;
			  $result = $user->regiser($username,$password,$re_password,$email);
			  if($result >0){
				switch($result){
					case 1:
						showErr("Please input full information");
						break;
					case 2:
						showErr("The two passwords do not match");
						break;
					case 3:
						showErr("Username already exists");
						break;
					case 4:
						showErr("Email already exists");
						break;
					case 5:
						showErr("Unsuccess to register");
						break;
					default:
						showErr("Error system, please try again");
						break;
					}
				}
				else{					
					//set new session
					session_start();
					$_SESSION[SESSION_USER] = $username;
					$_SESSION[SESSION_TIME_LOGGIN] = time();
					header('location: main.php');
				}
		  	}
	   ?>
  	<div class="input-group">
  	  <label>Username</label>
  	  <input type="text" name="username" value="<?php echo $user->username; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Email</label>
  	  <input type="email" name="email" value="<?php echo $user->email; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Password</label>
  	  <input type="password" name="password">
  	</div>
  	<div class="input-group">
  	  <label>Confirm password</label>
  	  <input type="password" name="re_password">
  	</div>
  	<div class="input-group">
  	  <button type="submit" class="btn" name="reg_user">Register</button>
  	</div>
  	<p>
  		Already a member? <a href="login.php">Sign in</a>
  	</p>
  </form>
</body>
</html>