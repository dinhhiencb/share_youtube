<?php
    include('config/define.php');
    session_start();
    if (!(isset($_SESSION[SESSION_USER]) && $_SESSION[SESSION_USER] != '')) {
        header ("Location: views/login.php");    
        //echo ("not login");
    }
    else{
        header ("Location: views/main.php");    
    }
    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION[SESSION_USER]);
        unset($_SESSION[SESSION_TIME_LOGGIN]);
        header("location: views/login.php");
    }
?>