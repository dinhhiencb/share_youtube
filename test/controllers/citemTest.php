<?php
    require_once '../../models/m_item.php';
    require_once '../../controllers/ctr_item.php';
    require_once '../../config/define.php';
    use PHPUnit\Framework\TestCase;
    class citemTest extends TestCase{
        public function testloginUserFail() {
            $itemTest = new CItem();
            $this->assertSame(false,$itemTest->getItems());
        }
        public function testloginUserOK() {
            $itemTest = new CItem();
            $this->assertSame(true,$itemTest->getItems());
        }

        public function testinsertFailNull() {
            $itemTest = new CItem();
            $this->assertSame(1,$itemTest->insert("", "","",""));
        }
        public function testregisterUserFailPass() {
            $itemTest = new CItem();
            $this->assertSame(2,$itemTest->insert("admin", "title","description","https://youtube.com"));
        }
        public function testregisterUserOK() {
            $itemTest = new CItem();
            $this->assertSame(0,$itemTest->insert("admin", "title","description","https://youtube.com"));
        }
    }
?>