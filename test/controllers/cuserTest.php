<?php
    require_once '../../models/m_user.php';
    require_once '../../controllers/ctr_user.php';
    require_once '../../config/define.php';
    use PHPUnit\Framework\TestCase;
    class cuserTest extends TestCase{
        public function testloginUserFail() {
            $userTest = new CUser();
            $this->assertSame(false,$userTest->login("admin", "wrong_pass"));
        }
        public function testloginUserOK() {
            $userTest = new CUser();
            $this->assertSame(true,$userTest->login("admin", "123"));
        }

        public function testregisterUserFailNull() {
            $userTest = new CUser();
            $this->assertSame(1,$userTest->regiser("", "","",""));
        }
        public function testregisterUserFailPass() {
            $userTest = new CUser();
            $this->assertSame(2,$userTest->regiser("admin123", "1232","123","admin@admin"));
        }
        public function testregisterUserFailUserExist() {
            $userTest = new CUser();
            $this->assertSame(3,$userTest->regiser("admin", "123","123","admin@admin"));
        }
        public function testregisterUserFailEmailExist() {
            $userTest = new CUser();
            $this->assertSame(4,$userTest->regiser("admin", "123","123","admin@"));
        }
        public function testregisterUserOK() {
            $userTest = new CUser();
            $this->assertSame(0,$userTest->regiser("admin123", "123","123","admin@admin"));
        }
    }
?>