-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2019 at 06:15 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `share_youtube`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `user` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `user`, `title`, `description`, `url`) VALUES
(1, 'admin', 'F | MD | GIDEON/SUKAMULJO (INA [1] vs. AHSAN/SETIAWAN (INA) [2] | BWF 2019', 'Disclaimer: If you are not able to watch the live stream of matches on http://badmintonworld.tv due to it being geo-blocked in your country that is because the rights for the World Championships has been sold to a broadcaster in your country.', 'https://www.youtube.com/watch?v=nYRwDgfWo60'),
(2, 'admin', 'F | WD | CHEN/JIA (CHN) [4] vs. MATSUTOMO/TAKAHASHI (JPN) [2] | BWF 2019', 'Disclaimer: If you are not able to watch the live stream of matches on http://badmintonworld.tv due to it being geo-blocked in your country that is because the rights for the World Championships has been sold to a broadcaster in your country.', 'https://www.youtube.com/watch?v=fa--63s8hUs'),
(3, 'admin', 'F | MS | Kento MOMOTA (JPN) [1] vs. Anthony Sinisuka GINTING (INA) [7] | BWF 2019', 'Disclaimer: If you are not able to watch the live stream of matches on http://badmintonworld.tv due to it being geo-blocked in your country that is because the rights for the World Championships has been sold to a broadcaster in your country.', 'https://www.youtube.com/watch?v=zPAuJIkpR3k&t=1s'),
(4, 'admin', 'F | WS | Carolina MARIN (ESP) vs. TAI Tzu Ying (TPE) [1] | BWF 2019', 'Disclaimer: If you are not able to watch the live stream of matches on http://badmintonworld.tv due to it being geo-blocked in your country that is because the rights for the World Championships has been sold to a broadcaster in your country', 'https://www.youtube.com/watch?v=SbczMu4zNP4');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(1, 'admin', 'admin@admin.com', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
