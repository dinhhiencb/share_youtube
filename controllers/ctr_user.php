<?php
    class CUser extends CMUser{
        public $id = null;
        public $username = null;
        public $email = null;
        public $errors = array();

        //login to manage website
        function login($username,$password){
            $user = $this->loginUser($username,$password);
            if($user != null){
                //parse info user
                $this->id = $user[ID];
                $this->username = $user[USERNAME];
                $this->email = $user[EMAIL];
                return true;
            }
            else {return false;}
        }

        function regiser($username,$password,$re_password,$email){
            $errors = array();
            if($username === null || $password === null || $re_password === null || $email === null){
                return 1;
            }
            if ($password != $re_password) {
                return 2;
            }
            $user = $this->checkExistUser($username,$email);
            if($user != null){
                $this->username = $user[USERNAME];
                $this->email = $user[EMAIL];
                if($this->username === $username){
                    return 3;
                }
                if($this->email === $email){
                    return 4;
                }
            }
            if($this->regiserUser($username,$password,$email) != 0){
                return 5;
            }
            else{
                return 0;
            }
        }
    }
?>