<?php
    class CItem extends CMItem{
        private $htmlItems = null;
        public $items = array();

        #http://img.youtube.com/vi/<YouTube_Video_ID_HERE>/mqdefault.jpg
        #https://www.youtube.com/watch?v=Q-GYwhqDo6o
        function getItems(){
            $row = $this->getAllItems();
            if($row != null){
                foreach($row as $item){

                    $this->items[$item[ID]][ID] = $item[ID];
                    $this->items[$item[ID]][USER] = $item[USER];
                    $this->items[$item[ID]][TITLE] = $item[TITLE];
                    $this->items[$item[ID]][DES] = $item[DES];
                    $this->items[$item[ID]][URL] = $item[URL];
                    
                    $link = $item[URL];
                    $video_id = explode("?v=", $link);
                    if (!isset($video_id[1])) {
                        $video_id = explode("youtu.be/", $link);
                    }
                    $youtubeID = $video_id[1];
                    if (empty($video_id[1])) $video_id = explode("/v/", $link);
                    $video_id = explode("&", $video_id[1]);
                    $youtubeVideoID = $video_id[0];
                    $this->items[$item[ID]][URL_ID] = "http://img.youtube.com/vi/" . $youtubeVideoID . "/mqdefault.jpg";
                }
                return true;
            }
            else {return false;}
        }

        function insert($user,$title, $description,$url){
            if($user === null || $title === null || $description === null || $url === null){
                return 1;
            }
            if($this->insertItem($user,$title, $description,$url) != 0){
                return 2;
            }
            else{
                return 0;
            }
        }
    }
?>